---
title: Git Tutorial 3/4
subtitle: Using Git to implement a new feature/change without affecting the master branch
date: 2019-11-28
tags: ["example", "code"]
---

With git being a distributed source code management tool it is possible to any member of a team to work locally on a project and later to merge up all the contributions together. To get introduced to source code management read the first part of this Git series [Git Tutorial 1/4](/post/git-tut-1-4)

Git tracks source code on branches, like tree's branches. These are forks of the original source where people try new features or fix a bug and test it before merging it back to a main branch.
Here how a git branch forks out from a main 'Master' branch and then converges back to the original branch. ([img src](https://wac-cdn.atlassian.com/dam/jcr:83323200-3c57-4c29-9b7e-e67e98745427/Branch-1.png)):
<!--more-->

![how a git branch looks like](/img/git-branch-1.png)

This is the basic workflow we will follow in our tutorial. So we will get a local copy of the project repository, add the new feature, then send it back to be merged with the master project branch.

first, you need to install git on your machine. Follow instructions in [Git Tutorial 2/4](/post/git-tut-2-4) if you haven't git already installed.

### Clone the repository locally 

Git uses subcommands for different tasks. use "git help *subcommand*" to get help about a particular command.
The 'clone' command creates a directory with the same name as the repository name (ending with .git in the url) and gets a copy of source code from the repository url. Here is the command for a sample project on github. substitue with your project's repository url
```
 git clone https://gitserver.com/project/project.git
```
### Create a new branch

Create Your own branch to work on so we don't touch to Master branch, we'll give it 'feature-branch' as a name:
```
git checkout -b feature-branch
```
now we are working on the 'feature-branch' branch and not on 'Master', and as 'feature-branch' didn't exist before it has been automatically created by the above command.
### Add your Feature

Edit your source code by edit/delete existing files or adding new ones. for how to add a file to git read previous blog post [Git Tutorial 2/4](/post/git-tut-2-4)

finish your changes with a commit:
```
git commit -a -m "new feature"
```
with -a we told git to automatically stage modified and deleted files.<br>
with -m we added a message to the commit.

look at your branches structure:
```
git log --graph --decorate --abbrev-commit --all --pretty=oneline
```

you'll get something like:
```
| * 6c4abf5 (feature-branch) new feature
* | 9594751 second commit
|/  
* 57c584d first commit
```
### Push your Feature
Now push your branch:
```
git push origin feature-branch
```
'origin' is a git alias of the repository url from which we have cloned our project. 'feature-branch' is the branch name we want push to server.


### Merge the new Feature

The project maintainer (may be you :) ) who has commit access to the Master branch on the server will have to merge your work.
Typically he will have to update his project git repository (especially getting your branch and changes):
```
git pull
```
then he would merge your work:
```
git merge feature-branch
```
Once done the git log command above would now show:
```
*   5017320 merge feature-branch
|\  
| * 6c4abf5 (feature-branch) new feature
* | 9594751 second commit
|/  
* 57c584d first commit
```
**NOTE**: the maintainer would very likely have to resolve merge conflicts before this step succeeds.
<br>Merge conflicts arise from changes made to the same file at the same line in two different branches. This way git can't decide which change to keep and which to discard or may be whether to keep a combination of both.
so the maintainer has to decide for git.<br>
Stay tuned for the next [Git Tutorial 4/4](/post/git-tut-4-4) where we'll learn more about resolving merge conflicts.



